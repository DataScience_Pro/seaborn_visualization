# README
This is Data Visualization Project with Seaborn and Matplotlib.

## Visualization Images
- Anscombe

![Anscombe](image/anscombe.PNG)

- Cubehelix

![Cubehelix](image/cubehelix.PNG)

- Distribute Plot

![Distplot](image/distplot.PNG)

- Violin Plot

![ViolinPlot](image/violinplot.PNG)

- Facet Grid

![FacetGrid](image/facetgrid.PNG)

- Grouped Box Plot

![GroupedBoxPlot](image/grouped_boxplot.PNG)

- Heatmap

![HeatMap](image/heatmap.PNG)

- Hexbin Plot

![HexbinPlot](image/hexbin_plot.PNG)

- Horizontal Box Plot

![HorizontalBoxPlot](image/horizontal_boxplot.PNG)

- Strip Plot

![StripPlot](image/stripplot.PNG)

- Joint KDE(Kernel Density Estimation)

![JointKDE](image/joint_kde.PNG)

- KDE Joy Plot

![KDEJoyPlot](image/kde_joyplot.PNG)

- Large Distribution

![LvPlot](image/lvplot.PNG)

- Logistic Regression

![LogisticRegression](image/logistic_regression.PNG)

- Many Facet

![ManyFacet](image/many_facet.PNG)

- Pairwise Correlation

![Pairwise](image/pairwise_correlation.PNG)

- Multiple KDE

![MultipleKDE](image/multiple_kde.PNG)

- Multiple Regression

![MultipleRegression](image/multiple_regression.PNG)

- Pair Grid

![PairGrid](image/pair_grid.PNG)

- PairGrid Dot Plot

![PairGridDotPlot](image/pairgrid_dotplot.PNG)

- PointPlot Anova

![PointPloatAnova](image/pointplot_anova.PNG)

- ResidPlot

![ResidPlot](image/residplot.PNG)

- Scatter Plot

![ScatterPlot](image/scatterplot.PNG)

- Structured Heatmap

![StructuredHeatmap](image/structured_heatmap.PNG)

- TimeSeries

![TimeSeries](image/timeseries.PNG)